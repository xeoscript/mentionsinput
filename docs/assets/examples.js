$(function () {
  var data = [
    { id:1, abrev: 'MOB', name:'Maítas orellana', 'avatar':'https://trello-avatars.s3.amazonaws.com/72f42e7ad39f5ab3c503f5a85e9e390a/50.png', 'type':'contact' },
    { id:2, abrev: 'Ameboide', name:'Javier López', 'avatar':'http://cdn0.4dots.com/i/customavatars/avatar7112_1.gif', 'type':'contact' },
    { id:3, abrev: 'KZ', name:'Ariel', 'avatar':'http://cdn0.4dots.com/i/customavatars/avatar7112_1.gif', 'type':'contact' },
    { id:4, abrev: 'ABX', name:'Abraham Barrera', 'avatar':'https://trello-avatars.s3.amazonaws.com/7a1735fb10474942ff1f953699551c92/30.png', 'type':'contact' },
    { id:5, abrev: 'FER', name:'Fernando Lenero', 'avatar':'https://trello-avatars.s3.amazonaws.com/63eb14a35c039b22e99b123676b32602/50.png', 'type':'contact' },
    { id:5, abrev: 'FAYCA', name:'Facio y Cañas Abogados', 'avatar':'https://trello-avatars.s3.amazonaws.com/63eb14a35c039b22e99b123676b32602/50.png', 'type':'client' },
    { id:5, abrev: 'OLIVA', name:'Estudio de abogados Olivares', 'avatar':'https://trello-avatars.s3.amazonaws.com/63eb14a35c039b22e99b123676b32602/50.png', 'type':'client' },
    { id:5, abrev: 'OLIVE', name:'Olivera y problemas con CRON', 'avatar':'https://trello-avatars.s3.amazonaws.com/63eb14a35c039b22e99b123676b32602/50.png', 'type':'client' },
  ];

  $('textarea.mention#mention1').mentionsInput({
    onDataRequest:function (mode, query, callback) {
      data_filtered = _.filter(data, function(item) {
        return item.abrev.toLowerCase().indexOf(query.toLowerCase()) > -1
      });
      callback.call(this, data_filtered);
    }
  });

  $('textarea.mention#mention2').mentionsInput({
    onDataRequest:function (mode, query, callback) {
      var data_type = $(this).data('type');
      data_filtered = _.filter(data, function(item) {
        return (item.abrev.toLowerCase().indexOf(query.toLowerCase()) > -1) && ((data_type && item.type == data_type) || !data_type)
      });
      callback.call(this, data_filtered);
    }
  });

  $('.get-syntax-text').click(function() {
    alert($('textarea.mention').val());
  });

  $('.get-mentions').click(function() {
    $('textarea.mention').mentionsInput('getMentions', function(data) {
      alert(JSON.stringify(data));
    });
  }) ;

});